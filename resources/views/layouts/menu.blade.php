<nav class="navbar navbar-expand-md shadow-sm navbar-background">
   <div class="container">
      <a class="navbar-brand" href="{{ url('/') }}">                    
      <img class="logo" src="{{ get_company_logo() }}" alt="{{ config('app.name', 'ProWriter') }}">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
      <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
         <ul class="navbar-nav mr-auto">
            @role('admin') 
            <li class="nav-item">
               <a class="nav-link" href="{{ route('dashboard') }}">Dashboard</a>
            </li>
            <li class="nav-item">
               <a class="nav-link" href="{{ route('orders_list') }}">Orders</a>
            </li>
            <li class="nav-item">
               <a class="nav-link" href="{{ route('users_list', ['type' => 'customer']) }}">Customers</a>
            </li>
            <li class="nav-item">
               <a class="nav-link" href="{{ route('users_list', ['type' => 'staff']) }}">
               Writers
               </a>
            </li>
            <li class="nav-item dropdown">
               <a id="managerial" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
               Manage <span class="caret"></span>
               </a>
               <div class="dropdown-menu" aria-labelledby="managerial">
                  <a class="dropdown-item" href="{{ route('bills_list') }}">Bills</a>
                  <a class="dropdown-item" href="{{ route('settings_main_page') }}">Settings</a>
                  <a class="dropdown-item" href="{{ route('users_list', ['type' => 'admin']) }}">Admin Users</a>
               </div>
            </li>
            <li class="nav-item">
               <a class="nav-link" href="{{ route('income_statement') }}">Income</a>
            </li>
            @endrole
            @role('staff')    
            @if(strtolower(settings('enable_browsing_work')) == 'yes')
            <li class="nav-item">
               <a class="nav-link" href="{{ route('browse_work') }}">Browse Work</a>
            </li>
            @endif
            <li class="nav-item">
               <a class="nav-link" href="{{ route('tasks_list') }}">My Tasks</a>
            </li>
            <li class="nav-item dropdown">
               <a id="payment_request" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
               Payment Request <span class="caret"></span>
               </a>
               <div class="dropdown-menu" aria-labelledby="payment_request">
                  <a class="dropdown-item" href="{{ route('request_for_payment') }}">
                  Request for payment
                  </a>
                  <a class="dropdown-item" href="{{ route('my_requests_for_payment') }}">
                  List of payment requests
                  </a>
               </div>
            </li>
            @endrole
            @auth
            @unlessrole('admin')
            <li class="nav-item">
               <a class="nav-link" href="{{ route('my_orders') }}">My Orders</a>
            </li>
            <li class="nav-item">
               <a class="nav-link" href="{{ route('order_page') }}">New Order</a>
            </li>
            @endunlessrole
            @endauth
         </ul>

         <ul class="navbar-nav ml-auto">         
            @guest
            <li class="nav-item">
               <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
            </li>
            @if (Route::has('register'))
            <li class="nav-item">
               <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
            </li>
            @endif
            @else
            @hasanyrole('staff|admin')
            <li class="nav-item dropdown">                                
               @include('layouts.notification_bell')
            </li>
            @endhasanyrole
            <li class="nav-item dropdown">
               <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
               {{ Auth::user()->first_name }} <span class="caret"></span>
               </a>
               <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="{{ route('my_account') }}">                   
                  My Account
                  </a>
                  @hasrole('admin')
                  <a class="dropdown-item" href="{{ route('my_orders') }}">My Orders</a>    
                  <a class="dropdown-item" href="{{ route('order_page') }}">New Order</a>
                  <div class="dropdown-divider"></div>
                  @endhasrole
                  <a class="dropdown-item" href="{{ route('logout') }}"
                     onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();">
                  {{ __('Logout') }}
                  </a>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                     @csrf
                  </form>
               </div>
            </li>
            @endguest
         </ul>
      </div>
   </div>
</nav>