@extends('layouts.app')
@section('title', $data['title'])
@section('content')
<div class="container page-container" id="app">
 <h2>{{ $data['title'] }}</h2>
 <br>
  <order :services="{{ json_encode($data['service_id_list']) }}"
:levels="{{ json_encode($data['work_level_id_list']) }}"
:urgencies="{{ json_encode($data['urgency_id_list']) }}"
:spacings="{{ json_encode($data['spacings_list']) }}"
:additional_services="{{ json_encode($data['additional_services_id_list']) }}"
:user_id="{{ (!Auth::guest()) ? auth()->user()->id : 'false' }}"
:restricted_order_page_url="'{{ route('order_page') }}'"
:upload_attachment_url="'{{ route('order_upload_attachment') }}'"
:add_to_cart="'{{ route('add_to_cart') }}'"
:create_account_url="'{{ route('register') }}'"
 ></order>
</div>
@endsection
