@extends('layouts.app')
@section('title', 'Checkout')
@section('content')
<div class="container page-container">
   <div class="row">
      <div class="col-md-12">
         <h3>Checkout</h3>
         @php 
         if($errors->has('payment_method_nonce')) 
         { 
         echo 	$errors->first('payment_method_nonce') ; 
         } 
         @endphp
         <hr>
      </div>
      <div class="col-md-6 d-none d-lg-block">
         <div class="checkout-image-cover">
            <img src="{{ asset('images/payment.svg') }}" class="img-fluid">	
         </div>
      </div>
      <div class="col-md-6">
         <div class="card">
            <div class="card-body">
               <div class="d-flex justify-content-between">
                  <h4 class="h4">Total</h4>
                  <div class="h4">{{ format_money($data['total']) }}</div>
               </div>
               <hr>
               <div id="dropin-wrapper">
                  <div id="checkout-message"></div>
                  <div id="dropin-container"></div>
                  <button style="display: none;" id="submit-button" class="btn btn-success btn-lg btn-block"><i class="fas fa-shopping-cart"></i> Confirm Order</button>
               </div>
               <div class="text-center" id="loading">Please wait while we loading the payment options ...</div>
            </div>
         </div>
      </div>
   </div>
   <form id="payment-form" method="POST" action="{{ route('checkout_process') }}">
      @csrf
      <input id="nonce" name="payment_method_nonce" type="hidden" />
      <input id="payment_method" name="payment_method" type="hidden" />
   </form>
</div>

@endsection

@push('scripts')

<!-- includes the Braintree JS client SDK -->
<script src="https://js.braintreegateway.com/web/dropin/1.22.1/js/dropin.min.js"></script>

<script>
  var button 	= document.querySelector('#submit-button');
  var loading 	= document.querySelector('#loading');

  braintree.dropin.create({
    // Insert your tokenization key here
    authorization: "{{ $data['client_token'] }}",
    container: '#dropin-container',
    paypal: {
    	flow: 'vault'
  	}

  }, function (createErr, instance) {

  	loading.style.display = 'none';

  	if (createErr) {
	    alert("Something went wrong");
	    return;
    }

    button.style.display = 'block';

    button.addEventListener('click', function () {
      instance.requestPaymentMethod(function (reqError, payload) {
        
        if (reqError) {
		    alert("Something went wrong");
		    return;
    	}

      	var form = document.querySelector('#payment-form');
      	document.querySelector('#nonce').value = payload.nonce;
        form.submit();

      });
    });

    activatePaymentMethodListener();
    

  });

  var creditCard = function() {
    
      document.getElementById('payment_method').value = 'credit_card';
  };

  var payPal = function() {
    
    document.getElementById('payment_method').value = 'paypal';
  };

  function activatePaymentMethodListener()
  {
      var cardElements = document.getElementsByClassName("braintree-option__card");

      for (var i = 0; i < cardElements.length; i++) {
        cardElements[i].addEventListener('click', creditCard, false);
      }

      var payPalElements = document.getElementsByClassName("braintree-option__paypal");

      for (var i = 0; i < payPalElements.length; i++) {
        payPalElements[i].addEventListener('click', payPal, false);
      }
  }


</script>

@endpush