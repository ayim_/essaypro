@extends('setup.index')
@section('title', 'Service & Pricing')
@section('setting_page')

@include('setup.partials.action_toolbar', [
 'title' => (isset($service->id)) ? 'Edit service' : 'Create new service', 
 'hide_save_button' => TRUE,
 'back_link' => ['title' => 'back to services', 'url' => route("services_list")],
])
   
<form role="form" class="form-horizontal" enctype="multipart/form-data" action="{{ (isset($service->id)) ? route( 'services_update', $service->id) : route('services_store') }}" method="post" autocomplete="off" >
   {{ csrf_field()  }}
   @if(isset($service->id))
   {{ method_field('PATCH') }}
   @endif
   <div class="form-group">
      <label>Name <span class="required">*</span></label>
      <input type="text" class="form-control form-control-sm {{ showErrorClass($errors, 'name') }}" name="name" value="{{ old_set('name', NULL, $service) }}">
      <div class="invalid-feedback">{{ showError($errors, 'name') }}</div>
   </div>
   
   <div class="form-row">
      <div class="form-group col-md-6">
      <label>Single Spacing Price <span class="required">*</span></label>
      <input type="text" class="form-control form-control-sm {{ showErrorClass($errors, 'single_spacing_price') }}" name="single_spacing_price" value="{{ old_set('single_spacing_price', NULL, $service) }}">
      <div class="invalid-feedback">{{ showError($errors, 'single_spacing_price') }}</div>
   </div>
   <div class="form-group col-md-6">
      <label>Double Spacing Price <span class="required">*</span></label>
      <input type="text" class="form-control form-control-sm {{ showErrorClass($errors, 'double_spacing_price') }}" name="double_spacing_price" value="{{ old_set('double_spacing_price', NULL, $service) }}">
      <div class="invalid-feedback">{{ showError($errors, 'double_spacing_price') }}</div>
   </div>

   </div>

   <div class="form-group">
      <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="inactive" name="inactive" value="1" {{ old_set('inactive', NULL, $service) ? 'checked="checked"' : '' }}>
         <label class="custom-control-label" for="inactive">Inactive</label>
      </div>
   </div>
   <input type="submit" name="submit" class="btn btn-success" value="Submit"/>
</form>
@endsection