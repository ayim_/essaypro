<section class="header-account-page pt-100 d-flex align-items-end navbar-background" data-offset-top="#header-main">
  <div class="container pt-4 pt-lg-0">
    <div class="row justify-content-end">
      <div class=" col-lg-8">
        <!-- Salute + Small stats -->
        <div class="row align-items-center mb-4">
          <div class="col-lg-8 col-xl-5 mb-4 mb-md-0">
            <span class="h2 mb-0 text-white d-block">{{ $user->full_name }}</span>
      
          </div>
          <div class="col-auto flex-fill d-none d-xl-block">
            <!-- Some Content here -->
          </div>
        </div>
        <!-- Account navigation -->
        <div class="d-flex">
          <a href="{{ route('users_edit', $user->id) }}" class="btn btn-icon btn-group-nav shadow btn-neutral">
            <span class="btn-inner--icon"><i class="far fa-user"></i></span>
            <span class="btn-inner--text d-none d-md-inline-block">Edit Profile</span>
          </a>
          <div class="btn-group btn-group-nav shadow ml-auto" role="group" aria-label="Basic example">
            <div class="btn-group" role="group">
              <a class="btn btn-neutral btn-icon" href="{{ route('user_profile', $user->id) }}"> 
                <i class="far fa-address-card"></i> Profile
              </a>
              <a class="btn btn-neutral btn-icon" href="{{ route('user_profile', $user->id) }}?group=orders">
                <i class="fas fa-shopping-cart"></i> Orders
              </a>
              <a class="btn btn-neutral btn-icon" href="{{ route('user_profile', $user->id) }}?group=tasks"><i class="fas fa-tasks"></i> Tasks</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>