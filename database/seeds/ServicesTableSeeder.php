<?php

use Illuminate\Database\Seeder;
use App\Service;
// use Faker\Generator as Faker;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	$faker = Faker\Factory::create();

    	foreach ($this->get_services() as $name) 
    	{
            $amount = $faker->randomFloat(2,30,100);

    		$service = Service::create([
                'name' => $name,          
                'single_spacing_price' => $amount + 10 ,
                'double_spacing_price' =>  $amount,
                ]);
            
    	}
    	
    }

    private function get_services()
    {
    	return [
    		'Tutoring',
'Copywriting',
'Argumentative Essay',
'Admission/Application Essay',
'Annotated Bibliography',
'Article',
'Assignment',
'Book Report/Review',
'Case Study',
'Capstone Project',
'Business Plan',
'Coursework',
'Dissertation',
'Dissertation Chapter - Abstract',
'Dissertation Chapter - Introduction Chapter',
'Dissertation Chapter - Literature Review',
'Dissertation Chapter - Methodology',
'Dissertation Chapter - Results',
'Dissertation Chapter - Discussion',
'Editing',
'Essay',
'Formatting',
'Lab Report',
'Math Problem',
'Movie Review',
'Multiple Choice Questions',
'Personal Statement',
'Non-word assignment',
'Outline',
'PowerPoint Presentation Plain',
'Poster / Map',
'PowerPoint Presentation with Speaker Notes',
'Proofreading',
'Paraphrasing',
'Report',
'Research Paper',
'Research Proposal',
'Scholarship Essay',
'Rewriting',
'Speech',
'Statistics Project',
'Term Paper',
'Thesis',
'Thesis Proposal',
'Resume Editing',
'All Application Job Package',
'Resume Writing',
'Cover Letter Writing'
];
    }
}
