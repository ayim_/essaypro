<?php
namespace App\Services;

use App\Braintree;

class BraintreeService
{

    public static function getInstance()
    {
        $braintree = Braintree::all()->first();

        return new \Braintree\Gateway([
            'environment' => $braintree->environment,
            'merchantId' => $braintree->merchant_id,
            'publicKey' => $braintree->public_key,
            'privateKey' => $braintree->private_key
        ]);
    }
}