<?php
namespace App\Services;

use App\Service;
use App\WorkLevel;
use App\Urgency;
use App\AdditionalService;
use App\Setting;

class CartService
{

    function getTotal($request)
    {
        $service = Service::find($request['service_id']);
        $workLevel = WorkLevel::find($request['work_level_id']);
        $urgency = Urgency::find($request['urgency_id']);

        if ((strtolower($request['spacing_type']) == 'double')) {
            $data['base_price'] = $service->double_spacing_price;
        } else {
            $data['base_price'] = $service->single_spacing_price;
        }

        $data['work_level_cost'] = ($data['base_price'] * $workLevel->percentage_to_add) / 100;

        $data['urgency_cost'] = ($data['base_price'] * $urgency->percentage_to_add) / 100;

        $data['number_of_pages'] = $request['number_of_pages'];

        $data['total_additional_services_cost'] = $this->getAdditionalServiceCosts($request['added_services']);

        return round($this->calculate($data), 2);
    }

    private function calculate(array $data)
    {
        extract($data);

        return round((($base_price + $work_level_cost + $urgency_cost) * $number_of_pages) + 
        $total_additional_services_cost, 2);
    }

    private function getAdditionalServiceCosts($added_services)
    {
        if (isset($added_services) && is_array($added_services) && count($added_services) > 0) {
            foreach ($added_services as $row) {
                $service_ids[] = $row['id'];
            }

            return $rate = AdditionalService::whereIn('id', $service_ids)->sum('rate');
        }

        return 0;
    }

    function pricing()
    {
        $record['work_levels'] = [];
        $record['pricings'] = [];
        $record['services'] = [];

        $services = Service::whereNull('inactive')->get();
        $workLevels = WorkLevel::orderBy('percentage_to_add', 'ASC')->get();
        $urgencies = Urgency::orderBy('percentage_to_add', 'ASC')->get();

        if ($services->count() > 0 && $workLevels->count() > 0 && $urgencies->count() > 0) {
            foreach ($services as $service) {
                $record['pricings'][$service->id] = $this->get_price_by_service($service, $workLevels, $urgencies);
            }

            $record['work_levels'] = $workLevels->toArray();
            $record['services'] = $services->toArray();
        }

        return $record;
    }

    private function get_price_by_service($service, $workLevels, $urgencies)
    {
        $base_price = $service->double_spacing_price;

        foreach ($urgencies as $urgency) {
            $data[] = [
                'name' => $urgency->value . ' ' . $urgency->type,
                'record' => $this->get_pricing_chart($workLevels, $urgency, $base_price)
            ];
        }

        return $data;
    }

    private function get_pricing_chart($workLevels, $urgency, $base_price)
    {
        foreach ($workLevels as $workLevel) {
            $costing['base_price'] = $base_price;
            $costing['work_level_cost'] = ($base_price * $workLevel->percentage_to_add) / 100;
            $costing['urgency_cost'] = ($base_price * $urgency->percentage_to_add) / 100;
            $costing['total_additional_services_cost'] = 0;
            $costing['number_of_pages'] = 1;

            $data[] = $this->calculate($costing);
        }

        return $data;
    }

    public function get_staff_payment_amount($order_total)
    {
        if (Setting::get_setting('enable_browsing_work') == 'yes') {
            // Calculate Staff payment
            $payment_value = Setting::get_setting('staff_payment_amount');

            if (Setting::get_setting('staff_payment_type') == 'percentage') {
                return round((($order_total * $payment_value) / 100), 2);
            } else {
                return $payment_value;
            }
        }
        return NULL;
    }
}