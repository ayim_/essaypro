<?php
namespace App\Services;

use App\Order;
use App\OrderAdditionalService;
use App\Attachment;
use App\NumberGenerator;

class OrderService
{

    function create($data)
    {
        $order = new Order();
        $order->number = NumberGenerator::gen('App\Order');
        $order->title = $data['title'];
        $order->instruction = $data['instruction'];
        $order->customer_id = $data['customer_id'];
        $order->service_id = $data['service_id'];
        $order->work_level_id = $data['work_level_id'];
        $order->work_level_percentage_to_add = $data['work_level_percentage_to_add'];
        $order->number_of_pages = $data['number_of_pages'];
        $order->spacing_type = $data['spacing_type'];
        $order->base_price = $data['base_price'];
        $order->urgency_id = $data['urgency_id'];
        $order->urgency_percentage_to_add = $data['urgency_percentage_to_add'];
        $order->dead_line = date("Y-m-d", strtotime($data['urgency']));

        if (isset($data['order_status_id'])) {
            $order->order_status_id = $data['order_status_id'];
        } else {
            $order->order_status_id = ORDER_STATUS_NEW;
        }

        $order->sub_total = $data['cart_total'];
        $order->discount = 0;
        $order->credit_applied = 0;
        $order->total = $data['cart_total'];
        $order->payment_method = $data['payment_method'];
        $order->transaction_id = $data['transaction_id'];
        $order->staff_payment_amount = $data['staff_payment_amount'];
        $order->save();

        $this->record_added_services($order, $data);
        $this->record_attachments($order, $data);

        return $order;
    }

    private function record_added_services(Order $order, $data)
    {
        if (isset($data['added_services']) && is_array($data['added_services']) && count($data['added_services']) > 0) {
            foreach ($data['added_services'] as $row) {
                $service = new OrderAdditionalService();
                $service->service_id = $row['id'];
                $service->type = $row['type'];
                $service->name = $row['name'];
                $service->rate = $row['rate'];
                $order->added_services()->save($service);
            }
        }
    }

    private function record_attachments(Order $order, $data)
    {
        if (isset($data['files_data']) && is_array($data['files_data']) && count($data['files_data']) > 0) {
            foreach ($data['files_data'] as $row) {

                if (isset($row['upload']['data']['name'])) {
                    $attachment = new Attachment();
                    $attachment->name = $row['upload']['data']['name'];
                    $attachment->display_name = $row['upload']['data']['display_name'];
                    $attachment->user_id = $data['customer_id'];

                    $order->attachments()->save($attachment);
                }
            }
        }
    }
}