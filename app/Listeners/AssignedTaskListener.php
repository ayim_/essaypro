<?php

namespace App\Listeners;

use App\Events\AssignedTask;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Notifications\TaskAssignedToYou;
use App\Follower;

class AssignedTaskListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AssignedTask  $event
     * @return void
     */
    public function handle(AssignedTask $event)
    {   
        $order = $event->order;
        $assigned_to = $event->assigned_to;

        // Add the assigner as a follower
        Follower::firstOrCreate([
            'order_id' => $order->id, 
            'user_id' => $event->assigned_by->id
        ]);

        // Log user's activity
        $subject = anchor($order->number, route('orders_show', $order->id));
        $to = anchor($assigned_to->full_name, route('user_profile', $assigned_to->id));        
        logActivity($order, 'assigned '. $subject . ' to '. $to);

        // Send notification to assignee
        $assigned_to->notify((new TaskAssignedToYou($event->assigned_by, $order)));       
   
    }
}
