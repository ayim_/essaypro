<?php

namespace App\Listeners;

use App\Events\OrderStatusChanged;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class OrderStatusChangedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderStatusChanged  $event
     * @return void
     */
    public function handle(OrderStatusChanged $event)
    {
        $order = $event->order;
        $previousStatus = $event->previousStatus;
        $newStatus = $event->newStatus;

        $state = 'from '. $previousStatus . ' to '. $newStatus; 
         // Log user's activity
        $subject = anchor($order->number, route('orders_show', $order->id));
        logActivity($order, 'changed status of '. $subject. ' '. $state);
    }
}
