<?php

namespace App\Listeners;

use App\Events\RequestedForRevision;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;
use App\Notifications\ClientRequestedForRevision;

class RequestedForRevisionListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RequestedForRevision  $event
     * @return void
     */
    public function handle(RequestedForRevision $event)
    {
        $order = $event->order;

        // Log user's activity
        $subject = anchor($order->number, route('orders_show', $order->id));
        logActivity($order, 'requested for revision '. $subject);              
        
        $followers = $order->followers->push($order->assignee);
        Notification::send($followers, new ClientRequestedForRevision($order));

        Notification::route('mail', company_notification_email())
           ->notify(new ClientRequestedForRevision($order));   
    }
}
