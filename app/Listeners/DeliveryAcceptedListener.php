<?php

namespace App\Listeners;

use App\Events\DeliveryAccepted;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;
use App\Notifications\OrderDeliveryAccepted;

class DeliveryAcceptedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DeliveryAccepted  $event
     * @return void
     */
    public function handle(DeliveryAccepted $event)
    {   
        $order = $event->order;

        // Log user's activity
        $subject = anchor($order->number, route('orders_show', $order->id));
        logActivity($order, 'accepted delivered item for '. $subject);

        // Send notification to the followers
        $followers = $order->followers->push($order->assignee);
        Notification::send($followers, new OrderDeliveryAccepted($order));
      
        // Prepare email addresses for sending notifications      
        Notification::route('mail', company_notification_email())
        ->notify(new OrderDeliveryAccepted($order));    
    
    }
}
