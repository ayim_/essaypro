<?php

namespace App\Listeners;

use App\Events\BillPaid;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Notifications\NewPaymentRequest;
use App\Notifications\PayoutProcessed;

class BillPaidListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BillPaid  $event
     * @return void
     */
    public function handle(BillPaid $event)
    {
        $event->bill->from->notify(new PayoutProcessed($event->bill));
    } 
}
