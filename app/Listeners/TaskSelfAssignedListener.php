<?php

namespace App\Listeners;

use App\Events\TaskSelfAssigned;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;
use App\Notifications\SelfAssignedTask;
use App\User;

class TaskSelfAssignedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TaskSelfAssigned  $event
     * @return void
     */
    public function handle(TaskSelfAssigned $event)
    {   
        $order = $event->order;

        // Log user's activity
        $subject = anchor($order->number, route('orders_show', $order->id));
        logActivity($order, 'self assigned '. $subject);

        $admins = User::role('admin')->get();

        // Send notification to the followers
        Notification::send($admins, new SelfAssignedTask($order, $event->user));

        Notification::route('mail', company_notification_email())
            ->notify(new SelfAssignedTask($order, $event->user));
    }
}
