<?php

namespace App\Listeners;

use App\Events\WorkSubmitted;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;
use App\Notifications\SubmittedTask;

class WorkSubmittedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  WorkSubmitted  $event
     * @return void
     */
    public function handle(WorkSubmitted $event)
    {
        $order  = $event->submittedWork->order;
        $submittedWork = $event->submittedWork;
        // Log user's activity
        $subject = anchor($order->number, route('orders_show', $order->id));
        logActivity($order, 'submitted work for '. $subject);

        // Send notification to the followers
        Notification::send($order->followers, new SubmittedTask($submittedWork));

       
        $emails = [company_notification_email(), $order->customer->email];        
        
        foreach ($emails as $email) 
        {
           Notification::route('mail', $email)->notify(new SubmittedTask($submittedWork));
        }       
    }
}
