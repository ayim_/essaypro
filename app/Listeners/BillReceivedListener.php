<?php

namespace App\Listeners;

use App\Events\BillReceived;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;
use App\Notifications\NewPaymentRequest;
use App\User;

class BillReceivedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BillReceived  $event
     * @return void
     */
    public function handle(BillReceived $event)
    {
        $bill = $event->bill;

        // Log user's activity
        $subject = anchor($bill->number, route('bills_show', $bill->id));
        logActivity($bill, 'requested for payment '. $subject);

        $admins = User::role('admin')->get();

        if($admins->count() > 0)
        {
            Notification::send($admins, new NewPaymentRequest($bill));
        }     
        
        Notification::route('mail', company_notification_email())
            ->notify(new NewPaymentRequest($bill));
    }
}
