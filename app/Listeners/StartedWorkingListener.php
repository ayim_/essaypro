<?php

namespace App\Listeners;

use App\Events\StartedWorking;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;
use App\Notifications\WorkingStarted;

class StartedWorkingListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  StartedWorking  $event
     * @return void
     */
    public function handle(StartedWorking $event)
    {
        $order = $event->order;

        // Log user's activity
        $subject = anchor($order->number, route('orders_show', $order->id));
        logActivity($order, 'started working on '. $subject);        

        // Send notification to the followers
        Notification::send($order->followers, new WorkingStarted($order));

        // Send notification to the company notification email
        Notification::route('mail', company_notification_email())
            ->notify(new WorkingStarted($order));
    }
}
