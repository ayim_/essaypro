<?php

namespace App\Listeners;

use App\Events\OrderReceived;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Notification;
use App\Notifications\NewOrder;
use Illuminate\Support\Facades\Mail;
use App\Mail\OrderSummary;
use App\User;

class OrderReceivedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderReceived  $event
     * @return void
     */
    public function handle(OrderReceived $event)
    {   
        $order = $event->order;

        // Send Order Summary to Customer
        Mail::to($order->customer)->send(new OrderSummary($order));


        $admins = User::role('admin')->get();

        // Send notification to the followers
        Notification::send($admins, new NewOrder($order));


        // Send Notification Email to Admin/Company
        Notification::route('mail', company_notification_email())
            ->notify(new NewOrder($order));
    }
}
