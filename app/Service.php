<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
// use Spatie\Activitylog\Traits\LogsActivity;

class Service extends Model
{
	// use LogsActivity;

	// protected static $logAttributes = [        
 //        'name',
 //        'single_spacing_price',
 //        'double_spacing_price',
 //        'inactive'
 //    ];

 //    protected static $logOnlyDirty = true;
    
    protected $fillable = [
        'id',
        'name',
        'single_spacing_price',
        'double_spacing_price',
        'inactive'
    ];
}