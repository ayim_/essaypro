<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use App\Service;
use App\WorkLevel;
use App\Urgency;
use Carbon\Carbon;
use App\OrderStatus;
use App\User;

class Order extends Model
{

    use SoftDeletes;
    use \App\Traits\TagOperation;

    protected $dates = [
        'deleted_at',
        'created_at',
        'dead_line'
    ];

    function status()
    {
        return $this->hasOne('App\OrderStatus', 'id', 'order_status_id');
    }

    public function attachments()
    {
        return $this->hasMany('App\Attachment');
    }

    function comments()
    {
        return $this->hasMany('App\Comment');
    }

    function followers()
    {
      return $this->belongsToMany(User::class, 'followers', 'order_id', 'user_id'); 
    }

    function isAFollower($user_id)
    {
        return $this->followers()->where('user_id', $user_id)->exists();
    }

    function added_services()
    {
        return $this->hasMany('App\OrderAdditionalService');
    }

    function service()
    {
        return $this->hasOne('App\Service', 'id', 'service_id');
    }

    function work_level()
    {
        return $this->hasOne('App\WorkLevel', 'id', 'work_level_id');
    }

    function assignee()
    {
        return $this->belongsTo('App\User', 'staff_id', 'id');
    }

    function customer()
    {
        return $this->belongsTo('App\User', 'customer_id', 'id');
    }

    public function rating()
    {
        return $this->hasOne('App\Rating');
    }

    function submitted_works()
    {
        return $this->hasMany('App\SubmittedWork');
    }

    function latest_submitted_work()
    {
        $attachments = $this->submitted_works()
            ->orderBy('id', 'DESC')
            ->get();

        if ($attachments->count() > 0) {
            return $attachments->first();
        }

        return $attachments;
    }

    function revisionUsed()
    {
        return $this->submitted_works()->where('needs_revision', 1)->count();
    }

    static function admin_dropdown()
    {
        $data['staff_list'] = [
            '' => 'Select'
        ] + User::role([
            'staff'
        ])->orderBy('first_name', 'ASC')
            ->select(DB::raw('CONCAT(first_name, " ", last_name) AS name'), 'id')
            ->pluck('name', 'id')
            ->toArray();

        $data['order_status_list'] = OrderStatus::orderBy('id', 'ASC')->pluck('name', 'id')->toArray();

        return $data;
    }

    static function task_dropdown()
    {
        $data['order_status_list'] = [
            '' => 'All'
        ] + OrderStatus::orderBy('id', 'ASC')->pluck('name', 'id')->toArray();

        $data['dead_line_list'] = [
            '' => 'N/A',
            'today' => 'Today',
            'tommorrow' => 'Tommorrow',
            'day_after_tommorrow' => 'The day after tommorrow'
        ];

        return $data;
    }

    static function dropdown()
    {
        $data['service_id_list'] = Service::orderBy('name', 'ASC')->whereNull('inactive')->get();

        $data['work_level_id_list'] = WorkLevel::orderBy('id', 'ASC')->whereNull('inactive')->get();

        $data['additional_services_id_list'] = AdditionalService::orderBy('id', 'ASC')->whereNull('inactive')->get();

        $urgencies = \App\Urgency::whereNull('inactive')
        ->orderBy('percentage_to_add', 'ASC')->get();

        $urgency_list = [];

        if ($urgencies->count() > 0) {
            foreach ($urgencies as $urgency) {
                $str = $urgency->value . ' ' . $urgency->type . ' / ';

                $str .= get_urgency_date($urgency->type, $urgency->value);

                $date = get_urgency_date($urgency->type, $urgency->value, 'Y-m-d');

                $urgency_list[] = [
                    'id' => $urgency->id,
                    'name' => $str,
                    'value' => $urgency->value,
                    'percentage_to_add' => $urgency->percentage_to_add,
                    'date' => $date
                ];
            }
        }
        $data['urgency_id_list'] = $urgency_list;

        $data['spacings_list'] = [
            [
                'id' => 'double',
                'name' => "Double-spaced"
            ],
            [
                'id' => 'single',
                'name' => "Single-spaced"
            ]
        ];

        return $data;
    }

    static function statistics($staff_id = NULL)
    {
        $orders = Order::select('order_status_id', DB::raw('count(*) as total'))->groupBy('order_status_id');
        if ($staff_id) {
            $orders->where('staff_id', $staff_id);
        }

        $orders = $orders->pluck('total', 'order_status_id');

        $statuses = OrderStatus::all();

        if ($statuses->count() > 0) {
            $statuses = $statuses->toArray();

            foreach ($statuses as $key => $status) {
                $statuses[$key]['value'] = (! isset($orders[$status['id']])) ? 0 : $orders[$status['id']];
            }

            $statuses = array_chunk($statuses, 6);
        }

        return $statuses;
    }
}