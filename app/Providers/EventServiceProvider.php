<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'Illuminate\Auth\Events\Login' => [
            'App\Listeners\LogSuccessfulLogin',
        ],
        'App\Events\OrderReceived' => [
            'App\Listeners\OrderReceivedListener',
        ],
        'App\Events\TaskSelfAssigned' => [
            'App\Listeners\TaskSelfAssignedListener',
        ],
        'App\Events\AssignedTask' => [
            'App\Listeners\AssignedTaskListener',
        ],
        'App\Events\CommentPosted' => [
            'App\Listeners\CommentPostedListener',
        ],
        'App\Events\WorkSubmitted' => [
            'App\Listeners\WorkSubmittedListener',
        ],
        'App\Events\BillReceived' => [
            'App\Listeners\BillReceivedListener',
        ],
        'App\Events\BillPaid' => [
            'App\Listeners\BillPaidListener',
        ],
        'App\Events\StartedWorking' => [
            'App\Listeners\StartedWorkingListener',
        ],
        'App\Events\DeliveryAccepted' => [
            'App\Listeners\DeliveryAcceptedListener',
        ],
        'App\Events\RequestedForRevision' => [
            'App\Listeners\RequestedForRevisionListener',
        ],
        'App\Events\OrderStatusChanged' => [
            'App\Listeners\OrderStatusChangedListener',
        ],
        'Illuminate\Notifications\Events\NotificationSent' => [
            'App\Listeners\NotificationSentListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
