<?php
namespace App\Http\Controllers;

use App\Order;
use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Services\CartService;
use Yajra\DataTables\Facades\DataTables;
use Carbon\Carbon;
use App\Events\CommentPosted;
use App\Events\DeliveryAccepted;
use App\Events\RequestedForRevision;
use App\Events\OrderStatusChanged;
use Illuminate\Support\Facades\DB;
use Mews\Purifier\Facades\Purifier;

class OrderController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function index()
    {
        $data = Order::admin_dropdown();
        $data['statistics'] = Order::statistics();

        $data['staff_list'] = [
            '' => 'All',
            'unassigned' => 'Unassigned'
        ] + $data['staff_list'];

        $data['order_status_list'] = [
            '' => 'All'
        ] + $data['order_status_list'];
        $data['dead_line_list'] = [
            '' => 'N/A',
            'today' => 'Today',
            'tommorrow' => 'Tommorrow',
            'day_after_tommorrow' => 'The day after tommorrow'
        ];
        return view('order.index', compact('data'));
    }

    public function datatable(Request $request)
    {
        $orders = Order::with([
            'assignee',
            'customer'
        ])->orderBy('id', 'DESC');

        return Datatables::eloquent($orders)->addColumn('customer_html', function ($order) {

            return view('order.partials.order_list_row', compact('order'))->render();
        })
            ->rawColumns([
            'customer_html'
        ])
            ->filter(function ($query) use ($request) {

            if ($request->order_number) {

                $query->where('number', $request->order_number);
            }

            if ($request->order_status_id) {

                $query->where('order_status_id', $request->order_status_id);
            }

            if ($request->customer_id) {

                $query->where('customer_id', $request->customer_id);
            }

            if ($request->staff_id) {

                if ($request->staff_id == 'unassigned') {
                    $query->whereNull('staff_id');
                } else {
                    $query->where('staff_id', $request->staff_id);
                }
            }

            if ($request->order_date) {

                list ($from, $to) = explode(' - ', $request->order_date);           

                $query->whereBetween(DB::raw('DATE(created_at)'), [$from, $to]);
            }

            if ($request->dead_line) {
                $now = Carbon::now();

                switch ($request->dead_line) {
                    case 'tommorrow':
                        $now->addDays(1);
                        break;
                    case 'day_after_tommorrow':
                        $now->addDays(2);
                        break;
                    default:
                        break;
                }

                $query->whereDate('dead_line', $now->toDateString('Y-m-d'));
            }
        })
            ->make(true);
    }

    function show(Order $order)
    {
        $data = [];

        if (auth()->user()->hasRole('admin')) {
            // Always allow an admin to visit the page
            $data = Order::admin_dropdown();
        } else if (auth()->user()->hasRole('staff')) {
            /*
             * Restrict a staff from accessing the page if:
             * 1. He is not the creator/customer of the order
             * 2. He is not the assignee of the order/task
             * 3. Browsing Work is not enabled
             */
            if (settings('enable_browsing_work') != 'yes') {
                if ($order->customer_id != auth()->user()->id && $order->staff_id != auth()->user()->id) {
                    abort(404);
                }
            }
        } else {
            /*
             * Not an admin or a staff, which means the user is a client
             * Restrict a client from accessing the page if he is not the owner of the order
             *
             */
            if ($order->customer_id != auth()->user()->id) {
                abort(404);
            }
        }

        return view('order.show', compact('order', 'data'));
    }

    public function quote(Request $request)
    {
        if (auth()->check()) {
            return redirect()->route('order_page');
        }

        $data = Order::dropdown();
        $data['title'] = 'Get an instant quote';
        return view('order.create', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $data = Order::dropdown();
        $data['title'] = 'Let\'s get started on your project!';

        return view('order.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function add_to_cart(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'service_id' => 'required',
            'work_level_id' => 'required',
            'work_level_percentage_to_add' => 'required',
            'number_of_pages' => 'required',
            'spacing_type' => 'required',
            'base_price' => 'required',
            'urgency_id' => 'required',
            'urgency' => 'required',
            'urgency_percentage_to_add' => 'required',
            'added_services' => 'nullable|array',
            'files_data' => 'nullable|array',
            'title' => 'required|max:255',
            'instruction' => 'required'
        ]);

        $validator->after(function ($validator) use ($request) {
            if (empty(Purifier::clean($request->input('title')))) {
                $validator->errors()->add('title', 'Please enter a valid title');
            }

            if (empty(Purifier::clean($request->input('instruction')))) {
                $validator->errors()->add('instruction', 'Please enter a valid instruction');
            }
        });


        if ($validator->fails()) {

            return response()->json([
                'errors' => $validator->errors()
            ]);
        }

        $data = $request->all();
        $cartService = new CartService();
        $data['cart_total'] = $cartService->getTotal($data);
        $data['staff_payment_amount'] = $cartService->get_staff_payment_amount($data['cart_total']);
        $data['title'] = Purifier::clean($request->input('title'));
        $data['instruction'] = Purifier::clean($request->input('instruction'));

        $request->session()->put('cart', $data);

        return response()->json([
            'status' => 'success',
            'redirect_url' => route('checkout_page')
        ]);
    }

    function post_comment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'message' => 'required',
            'order_id' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $order = Order::find($request->order_id);

        if (! auth()->user()->hasRole('admin')) {
            if (! in_array(auth()->user()->id, [
                $order->customer_id,
                $order->staff_id
            ])) {
                abort(404);
            }
        }

        if($message = Purifier::clean($request->input('message')))
        {
            $comment = new Comment();
            $comment->body = $message;
            $comment->user_id = auth()->user()->id;
            $order->comments()->save($comment);

            // Dispatching Event
            event(new CommentPosted($comment));
        }

        return redirect()->back();
    }

    function accept_delivered_item(Request $request, Order $order)
    {
        // Only the customer can mark it as accepted
        if (auth()->user()->id != $order->customer_id) {
            abort(404);
        }

        $validator = Validator::make($request->all(), [
            'submitted_work_id' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $order->order_status_id = ORDER_STATUS_COMPLETE;
        $order->save();

        // Dispatching Event
        event(new DeliveryAccepted($order));

        return redirect()->route('orders_rating', $order->id)->withSuccess('Thank you very much. Your order is now marked as complete');
    }

    function revision_request_page(Request $request, Order $order)
    {
        if(!isRevisionAllowed($order))
        {
            abort(404);
        }
        
        if ($order->order_status_id != ORDER_STATUS_SUBMITTED_FOR_APPROVAL) {
            return redirect()->route('orders_show', $order->id);
        }
       
        if (auth()->user()->id != $order->customer_id) {
            abort(404);
        }

        return view('order.revision_request', compact('order'));
    }

    function revision_request(Request $request, Order $order)
    {
        // Only the customer can mark it as accepted
        if (auth()->user()->id != $order->customer_id) {
            abort(404);
        }

        if ($order->order_status_id != ORDER_STATUS_SUBMITTED_FOR_APPROVAL) {
            return redirect()->route('orders_show', $order->id);
        }

        $validator = Validator::make($request->all(), [
            'message' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $submitted_work = $order->latest_submitted_work();

        if ($submitted_work->count() > 0) {
            $submitted_work->needs_revision = TRUE;
            $submitted_work->customer_message = Purifier::clean($request->message);
            $submitted_work->save();

            // Update Order Status
            $order->order_status_id = ORDER_STATUS_REQUESTED_FOR_REVISION;
            $order->save();

            // Dispatching Event
            event(new RequestedForRevision($order));
        }

        return redirect()->route('orders_show', $order->id);
    }

    public function change_status(Request $request, Order $order)
    {
        if (! auth()->user()->hasRole('admin')) {
            abort(404);
        }

        $validator = Validator::make($request->all(), [
            'order_status_id' => 'required'
        ], [

            'order_status_id.required' => 'Order status is required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $previous = $order->status->name;
        $order->order_status_id = $request->order_status_id;
        $order->save();
        $new = $order->status->name;

        // Dispatching Event
        event(new OrderStatusChanged($order, $previous, $new));       

        return redirect()->back()->withSuccess('Status updated');
    }

    public function follow(Request $request, Order $order)
    {
        $order->followers()->attach(auth()->user()->id);

        return redirect()->back()->withSuccess('Following');
    }

    public function unfollow(Request $request, Order $order)
    {
        $order->followers()->detach(auth()->user()->id);

        return redirect()->back()->withSuccess('Unfollowed');
    }
}