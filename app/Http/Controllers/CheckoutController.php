<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Services\BraintreeService;
use App\Events\OrderReceived;
use App\Services\OrderService;

class CheckoutController extends Controller
{

    public function checkout(Request $request)
    {
        $cart = $request->session()->get('cart');

        if (empty($cart)) {
            return redirect()->route('order_page');
        }

        try {

            $gateway = BraintreeService::getInstance();
            $data['client_token'] = $gateway->ClientToken()->generate();
            $data['total'] = $cart['cart_total'];

            return view('order.checkout')->with('data', $data);
        } catch (\Exception $e) {

            abort(404);
        }
    }

    function process(Request $request)
    {
        $cart = $request->session()->get('cart');

        if (empty($cart)) {
            return redirect()->route('order_page');
        }

        $validator = Validator::make($request->all(), [
            'payment_method_nonce' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $gateway = BraintreeService::getInstance();

        $result = $gateway->transaction()->sale([
            'amount' => $cart['cart_total'],
            'paymentMethodNonce' => $request->payment_method_nonce,
            'options' => [
                'submitForSettlement' => true
            ]
        ]);

        if (isset($result->success) && $result->success) {
            

            $orderService = new OrderService();

            $cart['customer_id'] = auth()->user()->id;          
            $cart['transaction_id'] = $result->transaction->id;

            switch ($request->payment_method) {
                case 'credit_card':
                        $cart['payment_method'] = 'Credit Card';
                    break;
                case 'paypal':
                        $cart['payment_method'] = 'PayPal';
                    break;                
                default:
                        $cart['payment_method'] = NULL;
                    break;
            }


            $order = $orderService->create($cart);
            $request->session()->forget('cart');

            // Dispatching Event
            event(new OrderReceived($order));

            return redirect()->route('orders_show', $order->id)->withSuccess('Your order has been received. You will be notified when your document is ready');
        } else {

            return redirect()->route('checkout_page')->withFail('Your payment was declined. Please try again');
        }
    }
}