<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;
use App\Service;

class ServiceController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('setup.service.index');
    }

    public function datatable(Request $request)
    {
        $services = Service::orderBy('name', 'ASC');

        if (! $request->include_inactive_items) {
            $services->whereNull('inactive');
        }

        return Datatables::eloquent($services)->editColumn('name', function ($service) {

            return '<a href="' . route('services_edit', $service->id) . '">' . $service->name . '</a>';
        })
            ->editColumn('single_spacing_price', function ($service) {
            return format_money($service->single_spacing_price);
        })
            ->editColumn('double_spacing_price', function ($service) {
            return format_money($service->double_spacing_price);
        })
            ->addColumn('status', function ($service) {
            return ($service->inactive) ? 'Inactive' : 'Active';
        })
            ->addColumn('action', function ($service) {

            return '<a class="btn btn-sm btn-danger delete-item" href="' . route('services_delete', $service->id) . '"><i class="fas fa-minus-circle"></i></a>';
        })
            ->rawColumns([
            'name',
            'single_spacing_price',
            'double_spacing_price',
            'status',
            'action'
        ])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $service = new \StdClass();

        return view('setup.service.create', compact('service'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:services',
            'single_spacing_price' => 'required|numeric|min:1',
            'double_spacing_price' => 'required|numeric|min:1'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $request['inactive'] = (isset($request->inactive)) ? TRUE : NULL;

        Service::create($request->all());

        return redirect()->back()->withSuccess('Successfully created a new service');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        return view('setup.service.create', compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:services,id,' . $id,
            'single_spacing_price' => 'required|numeric|min:1',
            'double_spacing_price' => 'required|numeric|min:1'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $request['inactive'] = (isset($request->inactive)) ? TRUE : NULL;

        Service::find($id)->update($request->only('name', 'single_spacing_price', 'double_spacing_price', 'inactive'));

        return redirect()->back()->withSuccess('Successfully updated the service item');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Service $service)
    {
        $redirect = redirect()->route('services_list');

        try {

            $service->delete();
            $redirect->withSuccess('Successfully deleted');
        } catch (\Illuminate\Database\QueryException $e) {
            $redirect->withFail('You cannot delete the service as it is associated with one or multiple orders');
        } catch (\Exception $e) {
            $redirect->withFail('Could not perform the requested action');
        }

        return $redirect;
    }
}
