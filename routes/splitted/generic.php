<?php
Route::get('/dashboard', 'DashboardController@index')
->name('dashboard'); 

// Handle File Uploads and Downloads
Route::get('/attachments/download', 'AttachmentController@download')
->name('download_attachment');

Route::post('/attachment/upload', 'AttachmentController@upload')
->name('order_upload_attachment');

Route::delete('/attachment/upload', 'AttachmentController@remove');
// End of Handle File Uploads and Downloads

Route::prefix('my-account')->group(function () {

    Route::get('/', 'MyAccountController@index')
    ->name('my_account');

    Route::patch('/edit-profile', 'MyAccountController@update_profile')
    ->name('update_my_profile');

    Route::patch('/change-password', 'MyAccountController@change_password')
    ->name('change_password');

    Route::post('/change-photo', 'MyAccountController@change_photo')
    ->name('change_photo');

    Route::get('orders', 'MyAccountController@orders')
    ->name('my_orders');

});


Route::prefix('orders')->group(function () {        
 
    Route::get('create', 'OrderController@create')
    ->name('order_page');
   
    Route::post('comment', 'OrderController@post_comment')
    ->name('post_comment');

    Route::post('{order}/deliverable/accept','OrderController@accept_delivered_item')
    ->name('accept_delivered_item')
    ->where('order', '[0-9]+');

    Route::get('{order}/revision/request', 'OrderController@revision_request_page')
    ->name('revision_request_page');

    Route::post('{order}/revision/request', 'OrderController@revision_request')
    ->name('post_revision_request')
    ->where('order', '[0-9]+');   

    Route::post('cart/add', 'OrderController@add_to_cart')
    ->name('add_to_cart');    
    
    Route::get('{order}', 'OrderController@show')
    ->name('orders_show')
    ->where('order', '[0-9]+');

    Route::get('{order}/rating', 'RatingController@create')
    ->name('orders_rating')
    ->where('order', '[0-9]+');

    Route::post('{order}/rating', 'RatingController@store')
    ->name('ratings_store')
    ->where('order', '[0-9]+');
   

});

Route::get('/checkout', 'CheckoutController@checkout')
    ->name('checkout_page');

Route::post('/checkout/process', 'CheckoutController@process')
    ->name('checkout_process');


Route::prefix('notifications')->group(function () { 

    Route::get('unread', 'NotificationController@get_unread_notifications')
    ->name('get_unread_notifications');

    Route::get('count', 'NotificationController@push_notification')
    ->name('get_push_notification_count');
   
    Route::get('/notifications/redirect/{id}', 'NotificationController@redirect_url')
        ->name('notification_redirect_url'); 

    Route::get('/all', 'NotificationController@index')
    ->name('notifications_index');

    Route::post('/all', 'NotificationController@paginate')
        ->name('datatable_notifications');
   
    Route::get('/mark/read/all', 'NotificationController@mark_all_notification_as_read')
        ->name('notification_all_mark_as_read');
});